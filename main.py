from flask import Flask

#instance of flask object
app = Flask(__name__)

# route to display helloworld message
@app.route('/tic-tac-toe')
def startGame():
    return 'Welcome to the tic-tac-toe game'

# method to run flask server
if __name__ == '__main__':
    app.run(debug=True)